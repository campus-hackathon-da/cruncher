const winston = require('winston');

module.exports = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'cruncher-error.log', level: 'error', timestamp: 'true' }),
  ],
});
