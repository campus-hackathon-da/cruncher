const relevantTypes = [
  // 'CommitCommentEvent',
  'CreateEvent',
  // 'DeleteEvent',
  // 'ForkEvent',
  // 'IssuesEvent',
  'MemberEvent',
  // 'PublicEvent',
  // 'PullRequestEvent',
  'WatchEvent',
];

function extendCommitCommentEvent(filtered, event) {
  // do nothing, covered by basic filter
}

function extendCreateEvent(filtered, event) {
  filtered.payload = {};
  filtered.payload.ref = event.payload.ref;
  filtered.payload.ref_type = event.payload.ref_type;
  filtered.payload.master_branch = event.payload.master_branch;
}

function extendForkEvent(filtered, event) {
  filtered.payload = {};
  filtered.payload.forkee = {};
  filtered.payload.forkee.id = event.payload.forkee.id;
  filtered.payload.forkee.name = event.payload.forkee.name;
  filtered.payload.forkee.full_name = event.payload.forkee.full_name;
  filtered.payload.forkee.owner = {};
  filtered.payload.forkee.owner.login = event.payload.forkee.owner.login;
  filtered.payload.forkee.owner.id = event.payload.forkee.owner.id;
  filtered.payload.forkee.fork = event.payload.forkee.fork;
  filtered.payload.forkee.created_at = event.payload.forkee.created_at;
  filtered.payload.forkee.updated_at = event.payload.forkee.updated_at;
  filtered.payload.forkee.pushed_at = event.payload.forkee.pushed_at;
}

function extendIssuesEvent(filtered, event) {
  filtered.payload = {};
  filtered.payload.action = event.payload.action;
  filtered.payload.issue = {};
  filtered.payload.issue.number = event.payload.issue.number;
  filtered.payload.issue.user = {};
  filtered.payload.issue.user.login = event.payload.issue.user.login;
  filtered.payload.issue.user.id = event.payload.issue.user.id;
  filtered.payload.issue.state = event.payload.issue.state;
  filtered.payload.issue.created_at = event.payload.issue.created_at;
  filtered.payload.issue.updated_at = event.payload.issue.updated_at;
  filtered.payload.issue.closed_at = event.payload.issue.closed_at;
}

function extendMemberEvent(filtered, event) {
  filtered.payload = {};
  filtered.payload.member = {};
  filtered.payload.member.login = event.payload.member.login;
  filtered.payload.member.id = event.payload.member.id;
  filtered.payload.action = event.payload.action;
}

function extendPublicEvent(filtered, event) {
  // do nothing
}

function extendPullRequestEvent(filtered, event) {
  filtered.payload = {};
  filtered.payload.action = event.payload.action;
  filtered.payload.pull_request = {};
  filtered.payload.pull_request.id = event.payload.pull_request.id;
  filtered.payload.pull_request.state = event.payload.pull_request.state;
  filtered.payload.pull_request.body = event.payload.pull_request.body;
  filtered.payload.pull_request.created_at = event.payload.pull_request.created_at;
  filtered.payload.pull_request.updated_at = event.payload.pull_request.updated_at;
  filtered.payload.pull_request.closed_at = event.payload.pull_request.closed_at;
  filtered.payload.pull_request.merged_at = event.payload.pull_request.merged_at;
  filtered.payload.pull_request.merged = event.payload.pull_request.merged;
}

function extendWatchEvent(filtered, event) {
  filtered.payload = {};
  filtered.payload.action = event.payload.action;
}

function basicFilter(event) {
  let eventRepository = event.repo || event.repository;

  return {
    id: event.id,
    type: event.type,
    repo: {
      id: eventRepository.id,
      name: eventRepository.name,
    },
    public: event.public,
    created_at: new Date(event.created_at),
  };
}

function hasRepositoryInformation(event) {
  return event.repo || event.repository;
}

module.exports = function filter(event) {
  // filter irrelevent events
  if (relevantTypes.indexOf(event.type) == -1) {
    return null;
  }

  // filter events not related to repositories
  if (!hasRepositoryInformation(event)) {
    return null;
  }

  // apply basic filter (shared fields)
  let filtered = basicFilter(event);

  // extend filtered object depending on type
  switch (event.type) {
    case 'CommitCommentEvent':
      extendCommitCommentEvent(filtered, event);
      break;
    case 'CreateEvent':
      extendCreateEvent(filtered, event);
      break;
    case 'ForkEvent':
      extendForkEvent(filtered, event);
      break;
    case 'IssuesEvent':
      extendIssuesEvent(filtered, event);
      break;
    case 'MemberEvent':
      extendMemberEvent(filtered, event);
      break;
    case 'PublicEvent':
      extendPublicEvent(filtered, event);
      break;
    case 'PullRequestEvent':
      extendPullRequestEvent(filtered, event);
      break;
    case 'WatchEvent':
      extendWatchEvent(filtered, event);
      break;
  }

  return filtered;
};
