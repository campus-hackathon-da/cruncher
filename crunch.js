#!/usr/bin/env node

const fs = require('fs');
const os = require('os');

const MongoClient = require('mongodb').MongoClient;

const config = require('./config');
const filter = require('./src/filter');
const logger = require('./src/logger');

// get file name from command line arguments
const filename = process.argv[2];

if (!filename) {
  logger.error('No filename given as argument. Stopping.');

  // cannot proceed without filename
  process.exit(1);
}

fs.readFile(filename, { encoding: 'utf-8' }, (err, data) => {
  if (err) {
    logger.error(`Cannot read file ${filename}`);
    return;
  }

  const dbUrl = `mongodb://${config.mongo.url}:27017/${config.mongo.db}`;

  MongoClient.connect(dbUrl, (err, db) => {
    if (err) {
      logger.error('Cannot connect to database', { error: err });
      return;
    }

    let events = db.collection('events');

    // split content into lines
    let lines = data.split(os.EOL);

    lines.forEach(line => {
      // skip empty lines
      if (!line) {
        return;
      }

      // parse line as JSON
      let event = null;
      try {
        event = JSON.parse(line);
      } catch (e) {
        logger.error('Cannot parse line', { exception: e });
        return;
      }

      // apply event filter to parsed line
      let filtered = filter(event);

      // if the filter matched, insert object into database
      if (filtered) {
        events.insert(filtered, (err, result) => {
          if (err) {
            logger.error('Cannot insert element into database', { error: err });
            return;
          }

          logger.info('Successfully inserted element into database');
        });
      }
    });

    db.close();
  });
});
